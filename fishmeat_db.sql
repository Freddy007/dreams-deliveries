-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 27, 2020 at 05:42 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u291070854_cooknew`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `add1` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `add2` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pincode` int(11) DEFAULT NULL,
  `city` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `uid`, `add1`, `add2`, `pincode`, `city`, `state`, `type`) VALUES
(3, 2, 'Royal Tower', 'Limbo Road, Canada', 96553, 'Toronto', 'Canada', 'Office');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'cook', 'cook@1234');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `banner_img` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `banner_img`, `status`) VALUES
(29, 'product/5ecd3d225908abanner1.jpg', 1),
(30, 'product/5ecd3d323824cbanner4.jpg', 1),
(31, 'product/5ecd3d4a2b10abanner.jpg', 1),
(32, 'product/5ecd3d5c7b61cbanner3.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `title`, `img`, `status`) VALUES
(6, 'Combos', 'product/5ecbb32c021fd1.png', 1),
(7, 'Chicken', 'product/5ecbb336bdb632.png', 1),
(8, 'Lamb & Goat', 'product/5ecbb34017ac83.png', 1),
(12, 'Sea Food', 'product/5ecbb3498df064.png', 1),
(13, 'Vegetables', 'product/5ecbb354a96ce5.png', 1),
(14, 'Marinades', 'product/5ecbb35e6ab546.png', 1),
(17, 'Exotic', 'product/5ecbb3698ef9e7.png', 1),
(18, 'Relishes', 'product/5ecbb379506218.png', 1),
(19, 'Snacks', 'product/5ecbb38449d3e9.png', 1),
(21, 'Eggs', 'product/5ecbed9cecba810.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `noti`
--

CREATE TABLE `noti` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `img` text NOT NULL,
  `msg` text NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sdesc` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `net` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gross` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `types` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `discount` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pipack` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `popular` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `cid`, `name`, `image`, `sdesc`, `net`, `gross`, `types`, `price`, `status`, `discount`, `pipack`, `popular`) VALUES
(32, 13, 'Fresh Potato', 'product/5ecb81a3a28f6shop-17-540x540.jpg', 'Fresh Potatoes are nutrient-dense, non-fattening and have reasonable amount of calories. Include them in your regular meals so that the body receives a good supply of carbohydrates', '1KG', '80', 'Pack', '36', 1, '10', '10+', 1),
(33, 13, 'Fresh Capsicum', 'product/5ecb8110adcc0shop-24-540x540.jpg', 'Leaving a moderately pungent taste on the tongue, Green capsicums, also known as green peppers are bell shaped, medium-sized fruit pods.', '500G', '40', 'Pack', '20', 1, '10', '4+', 1),
(49, 13, 'Lettuce', 'product/5ecb82aaa5c9eshop-4-540x540.jpg', 'Lettuce is an annual plant of the daisy family, Asteraceae. It is most often grown as a leaf vegetable, but sometimes for its stem and seeds. Lettuce is most often used for salads, although it is also seen in other kinds of food, such as soups, sandwiches and wraps; it can also be grilled.', '1KG', '50', 'Pieces', '50', 1, '20', '4-5', 0),
(50, 13, 'Organic Tomato', 'product/5ecb8454f1fb5shop-2-540x540.jpg', 'The fiber, potassium, vitamin C, and choline content in tomatoes all support heart health. An increase in potassium intake, along with a decrease in sodium intake, is the most important dietary change the average person can make to reduce their risk of cardiovascular disease. Tomatoes also contain folate', '2KG', '60', 'Pack', '60', 1, '10', '10-20', 0),
(52, 6, 'Greek Salad', 'product/5ecbc23bb19ac1.png', 'Greek salad or horiatiki salad is a popular salad in Greek cuisine generally made with pieces of tomatoes, cucumbers, onion, feta cheese, and olives and dressed with salt, pepper, Greek oregano, and olive oil. Common additions include green bell pepper slices or caper berries', '1 KG', '200', 'Pack', '80', 1, '20', '1', 0),
(53, 6, 'Pizza', 'product/5ecbc344718a82.png', 'Pizza is a savory dish of Italian origin, consisting of a usually round, flattened base of leavened wheat-based dough topped with tomatoes, cheese, and often various other ingredients baked at a high temperature, traditionally in a wood-fired oven. A small pizza is sometimes called a pizzetta.', '1 PCS', '100', 'Pieces', '200', 1, '30', '3', 1),
(54, 7, 'Grass fed chickens', 'product/5ecbc43a2e0b53.png', 'Grass fed chickens are those birds which are fed a pasture diet than a grain based diet. Pasture fed chickens forage for grass, seeds and a healthy portion of of insects. Chickens raised on a pasture are recommended by animal welfare groups and environmental organizations', '2KG', '200', 'Pieces', '50', 1, '20', '5', 0),
(55, 7, 'Chicken Dish', 'product/5ecbc4ea901314.png', 'Chicken Masala. Chicken is a Punjabi cuisine favorite. Teekha Murg. For all the spice fans! Murg Malaiwala. Kerala Chicken Roast. Spicy Tangy Kadhai Chicken. Chana Aur Khatte Pyaaz Ka Murgh. Butter Chicken', '200', '500', 'Pieces', '200', 1, '20', '20+', 0),
(56, 8, 'Goat meat Lamb and mutton', 'product/5ecbc5edba1685.png', 'Rogan Josh is a beautiful lamb curry with Persian roots. The lamb is cooked on a slow simmer to develop complex flavours and a beautiful red colour. Order our Lamb Curry Cut and get started on your Currylicious journey. Also explore our range of curry cuts of chicken, lamb/goat, and fish!', '50', '200', 'Pieces', '200', 1, '30', '10+', 0),
(57, 19, 'BBQ Beef Burger with French Fries', 'product/5ecbc6f3d11ad6.png', 'A hamburger is a sandwich consisting of one or more cooked patties of ground meat, usually beef, placed inside a sliced bread roll or bun. The patty may be pan fried, grilled, smoked or flame broiled.', '200', '30', 'Pieces', '200', 1, '0', '1', 1),
(58, 12, 'Fried chicken', 'product/5ecbc77d1f4dc7.png', 'Southern fried chicken, also known simply as fried chicken, is a dish consisting of chicken pieces which have been coated in a seasoned batter and pan-fried, deep fried, or pressure fried. The breading adds a crisp coating or crust to the exterior of the chicken while retaining juices in the meat', '500', '20', 'Pieces', '200', 1, '20', '20+', 0),
(59, 17, 'Spicy Chicken Fry', 'product/5ecbc88307cbe8.png', 'OMG! this is crazy-crisp, spicy and super good! Yes, the Chicken Fry that you will never be bored of. The juicy chicken pieces blanketed in a mix of basic spices, then deep fried to crisp outer and succulent within. A sibling to South Indian chicken kabab, this chicken recipe, I am sure will entice everyone. Give it a go guys, it’s worth the effort, Or I should say it is the effortless best chicken fry kababs.', '50', '200', 'Pieces', '200', 1, '20', '10+', 1),
(60, 21, 'Boiled egg', 'product/5ecbc907552bd9.png', 'Boiled eggs are eggs, typically from a chicken, cooked with their shells unbroken, usually by immersion in boiling water. Hard-boiled eggs are cooked so that the egg white and egg yolk both solidify, while soft-boiled eggs may leave the yolk, and sometimes the white, at least partially liquid and raw.', '500', '20', 'Pieces', '80', 1, '20', '20+', 0),
(61, 21, 'Egg Whites', 'product/5ecbc9d202d1b10.png', 'Protein. Eggs are a good source of high-quality, complete protein. Most of it is found in the egg white: There are 4 to 5 grams protein, 17 calories, and virtually no fat in a single large egg white. Egg whites are also a good source of leucine, an amino acid that may help with weight loss', '200', '20', 'Pieces', '250', 1, '30', '40+', 1),
(62, 19, 'French fries', 'product/5ecbcae4b886411.png', 'French fries are served hot, either soft or crispy, and are generally eaten as part of lunch or dinner or by themselves as a snack, and they commonly appear on the menus of diners, fast food restaurants, pubs, and bars. They are usually salted and, depending on the country, may be served with ketchup, vinegar, mayonnaise, tomato sauce, or other local specialties. Fries can be topped more heavily, as in the dishes of poutine or chili cheese fries.', '200', '50', 'Pack', '80', 1, '20', '1', 0),
(63, 18, 'Chicken Roll', 'product/5ecbcb5be424612.png', 'Chicken Roll is a delectable North Indian recipe made using all purpose flour, stir-fried chicken, yoghurt and a variety of vegetables rolled into paranthas. On days you do not want to prepare an elaborate meal, this delectable dish can be a saviour.', '10', '100', 'Pieces', '50', 1, '0', '1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE `register` (
  `id` int(11) NOT NULL,
  `email` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fname` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lname` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `pin` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `register`
--

INSERT INTO `register` (`id`, `email`, `password`, `mobile`, `city`, `fname`, `lname`, `status`, `pin`, `rdate`) VALUES
(2, 'cook@gmail.com', '123', '7878787878', 'Parish', 'fish', 'meat', 1, NULL, '2020-05-25 19:22:10');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `one_key` text NOT NULL,
  `one_hash` text NOT NULL,
  `contact_us` longtext NOT NULL,
  `privacy_policy` longtext NOT NULL,
  `about_us` longtext NOT NULL,
  `terms` longtext NOT NULL,
  `currency` text CHARACTER SET utf8 NOT NULL,
  `o_min` int(11) NOT NULL,
  `raz_key` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `one_key`, `one_hash`, `contact_us`, `privacy_policy`, `about_us`, `terms`, `currency`, `o_min`, `raz_key`) VALUES
(1, '067e1f42-2d53-434e-ba65-11733fa02e5d', 'ZWE2ODNkM2YtODE0Mi00OGE0LTk3NTUtNWM5NGMyMjhmMDQ0', '<p>Address Shop Number 67 68 69 Ground Floor Apple World Shopping Center, CA, 988852</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>WhatsApp or Call: +917276465975</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Skype: cscodetech</p>\r\n', '<p><strong>Privacy Policy</strong></p>\r\n\r\n<p>CSCODETECH built the Fish &amp; Meat Delivery app as a Free app. This SERVICE is provided by CSCODETECH at no cost and is intended for use as is.</p>\r\n\r\n<p>This page is used to inform visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service.</p>\r\n\r\n<p>If you choose to use our Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that we collect is used for providing and improving the Service. we will not use or share your information with anyone except as described in this Privacy Policy.</p>\r\n\r\n<p>The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at Fish &amp; Meat Delivery unless otherwise defined in this Privacy Policy.</p>\r\n\r\n<p><strong>Information Collection and Use</strong></p>\r\n\r\n<p>For a better experience, while using our Service, we may require you to provide us with certain personally identifiable information. The information that we request will be retained by us and used as described in this privacy policy.</p>\r\n\r\n<p>The app does use third party services that may collect information used to identify you.</p>\r\n\r\n<p>Link to privacy policy of third party service providers used by the app</p>\r\n\r\n<ul>\r\n	<li><a href=\"https://www.google.com/policies/privacy/\" target=\"_blank\">Google Play Services</a></li>\r\n	<li><a href=\"https://firebase.google.com/policies/analytics\" target=\"_blank\">Google Analytics for Firebase</a></li>\r\n	<li><a href=\"https://firebase.google.com/support/privacy/\" target=\"_blank\">Firebase Crashlytics</a></li>\r\n	<li><a href=\"https://onesignal.com/privacy_policy\" target=\"_blank\">One Signal</a></li>\r\n</ul>\r\n\r\n<p><strong>Log Data</strong></p>\r\n\r\n<p>we want to inform you that whenever you use our Service, in a case of an error in the app we collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (&ldquo;IP&rdquo;) address, device name, operating system version, the configuration of the app when utilizing our Service, the time and date of your use of the Service, and other statistics.</p>\r\n\r\n<p><strong>Cookies</strong></p>\r\n\r\n<p>Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers. These are sent to your browser from the websites that you visit and are stored on your device&#39;s internal memory.</p>\r\n\r\n<p>This Service does not use these &ldquo;cookies&rdquo; explicitly. However, the app may use third party code and libraries that use &ldquo;cookies&rdquo; to collect information and improve their services. You have the option to either accept or refuse these cookies and know when a cookie is being sent to your device. If you choose to refuse our cookies, you may not be able to use some portions of this Service.</p>\r\n\r\n<p><strong>Service Providers</strong></p>\r\n\r\n<p>we may employ third-party companies and individuals due to the following reasons:</p>\r\n\r\n<ul>\r\n	<li>To facilitate our Service;</li>\r\n	<li>To provide the Service on our behalf;</li>\r\n	<li>To perform Service-related services; or</li>\r\n	<li>To assist us in analyzing how our Service is used.</li>\r\n</ul>\r\n\r\n<p>we want to inform users of this Service that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.</p>\r\n\r\n<p><strong>Security</strong></p>\r\n\r\n<p>we value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.</p>\r\n\r\n<p><strong>Links to Other Sites</strong></p>\r\n\r\n<p>This Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by us. Therefore, we strongly advise you to review the Privacy Policy of these websites. we have no control over and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.</p>\r\n\r\n<p><strong>Children&rsquo;s Privacy</strong></p>\r\n\r\n<p>These Services do not address anyone under the age of 13. we do not knowingly collect personally identifiable information from children under 13. In the case we discover that a child under 13 has provided us with personal information, we immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact us so that we will be able to do necessary actions.</p>\r\n\r\n<p><strong>Changes to This Privacy Policy</strong></p>\r\n\r\n<p>we may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. we will notify you of any changes by posting the new Privacy Policy on this page.</p>\r\n\r\n<p>This policy is effective as of 2020-05-26</p>\r\n\r\n<p><strong>Contact Us</strong></p>\r\n\r\n<p>If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us at CSCODETECH.</p>\r\n', '<p>Your favourite grocery brand Fish&amp;Meat Delivery is now online as Fish&amp;Meat Delivery Ready. You can now buy your daily household need products that you find in a Fish&amp;Meat Delivery store here.</p>\r\n\r\n<p>We offer four types of delivery for your convenience:</p>\r\n\r\n<p>1) Pick up My self: Order online from the app and pick up your order from a Fish&amp;Meat Delivery Ready Pick Up point nearest to you. There is no delivery charge for collecting your order from a Fish&amp;Meat Delivery Ready Pick Up point.</p>\r\n\r\n<p>2) Home Delivery(COD): Place your order online and get it delivered to your home or office at a convenient date &amp; time of your choice. You will be charged a nominal delivery fee.</p>\r\n\r\n<p>3) Razorpay</p>\r\n\r\n<p>4) PayPal</p>\r\n\r\n<p><strong>Some of the key categories that we offer online:</strong></p>\r\n\r\n<p><br />\r\n- Daily grocery &amp; staples like Flours, Grains, Rice, Dals, Masalas, Spices, Cooking Oil, Ghee &amp; other daily need groceries, including Fish&amp;Meat Delivery Grocery.<br />\r\n- Fresh Fruits &amp; Vegetables<br />\r\n- Personal Care products like skin care, hair care, baby care and a variety of other personal utility &amp; hygiene products<br />\r\n- Household needs such as floor cleaners, cleaning tools, detergents, bathroom accessories, etc.<br />\r\n&nbsp;</p>\r\n\r\n<p><strong>App Features for easy Online Grocery Shopping</strong></p>\r\n\r\n<p><br />\r\n- Simplified design with easy navigation<br />\r\n- Appealing and persuasive user interface<br />\r\n- Faster browsing with just a click!<br />\r\n- Instant confirmation of receipt of your order<br />\r\n- Quick search and quicker check-outs<br />\r\n- Anywhere, anytime shopping convenience<br />\r\n- Innovative technology ensuring best-in-class customer experience<br />\r\n- Regular updates and notifications</p>\r\n', '<p>&nbsp;</p>\r\n\r\n<p><strong>Terms &amp; Conditions</strong></p>\r\n\r\n<p>By downloading or using the app, these terms will automatically apply to you &ndash; you should make sure therefore that you read them carefully before using the app. You&rsquo;re not allowed to copy, or modify the app, any part of the app, or our trademarks in any way. You&rsquo;re not allowed to attempt to extract the source code of the app, and you also shouldn&rsquo;t try to translate the app into other languages, or make derivative versions. The app itself, and all the trade marks, copyright, database rights and other intellectual property rights related to it, still belong to CSCODETECH.</p>\r\n\r\n<p>CSCODETECH is committed to ensuring that the app is as useful and efficient as possible. For that reason, we reserve the right to make changes to the app or to charge for its services, at any time and for any reason. We will never charge you for the app or its services without making it very clear to you exactly what you&rsquo;re paying for.</p>\r\n\r\n<p>The Fish &amp; Meat Delivery app stores and processes personal data that you have provided to us, in order to provide our Service. It&rsquo;s your responsibility to keep your phone and access to the app secure. We therefore recommend that you do not jailbreak or root your phone, which is the process of removing software restrictions and limitations imposed by the official operating system of your device. It could make your phone vulnerable to malware/viruses/malicious programs, compromise your phone&rsquo;s security features and it could mean that the Fish &amp; Meat Delivery app won&rsquo;t work properly or at all.</p>\r\n\r\n<p>The app does use third party services that declare their own Terms and Conditions.</p>\r\n\r\n<p>Link to Terms and Conditions of third party service providers used by the app</p>\r\n\r\n<ul>\r\n	<li><a href=\"https://policies.google.com/terms\" target=\"_blank\">Google Play Services</a></li>\r\n	<li><a href=\"https://firebase.google.com/terms/analytics\" target=\"_blank\">Google Analytics for Firebase</a></li>\r\n	<li><a href=\"https://firebase.google.com/terms/crashlytics\" target=\"_blank\">Firebase Crashlytics</a></li>\r\n	<li><a href=\"https://onesignal.com/tos\" target=\"_blank\">One Signal</a></li>\r\n</ul>\r\n\r\n<p>You should be aware that there are certain things that CSCODETECH will not take responsibility for. Certain functions of the app will require the app to have an active internet connection. The connection can be Wi-Fi, or provided by your mobile network provider, but CSCODETECH cannot take responsibility for the app not working at full functionality if you don&rsquo;t have access to Wi-Fi, and you don&rsquo;t have any of your data allowance left.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>If you&rsquo;re using the app outside of an area with Wi-Fi, you should remember that your terms of the agreement with your mobile network provider will still apply. As a result, you may be charged by your mobile provider for the cost of data for the duration of the connection while accessing the app, or other third party charges. In using the app, you&rsquo;re accepting responsibility for any such charges, including roaming data charges if you use the app outside of your home territory (i.e. region or country) without turning off data roaming. If you are not the bill payer for the device on which you&rsquo;re using the app, please be aware that we assume that you have received permission from the bill payer for using the app.</p>\r\n\r\n<p>Along the same lines, CSCODETECH cannot always take responsibility for the way you use the app i.e. You need to make sure that your device stays charged &ndash; if it runs out of battery and you can&rsquo;t turn it on to avail the Service, CSCODETECH cannot accept responsibility.</p>\r\n\r\n<p>With respect to CSCODETECH&rsquo;s responsibility for your use of the app, when you&rsquo;re using the app, it&rsquo;s important to bear in mind that although we endeavour to ensure that it is updated and correct at all times, we do rely on third parties to provide information to us so that we can make it available to you. CSCODETECH accepts no liability for any loss, direct or indirect, you experience as a result of relying wholly on this functionality of the app.</p>\r\n\r\n<p>At some point, we may wish to update the app. The app is currently available on Android &ndash; the requirements for system(and for any additional systems we decide to extend the availability of the app to) may change, and you&rsquo;ll need to download the updates if you want to keep using the app. CSCODETECH does not promise that it will always update the app so that it is relevant to you and/or works with the Android version that you have installed on your device. However, you promise to always accept updates to the application when offered to you, We may also wish to stop providing the app, and may terminate use of it at any time without giving notice of termination to you. Unless we tell you otherwise, upon any termination, (a) the rights and licenses granted to you in these terms will end; (b) you must stop using the app, and (if needed) delete it from your device.</p>\r\n\r\n<p><strong>Changes to This Terms and Conditions</strong></p>\r\n\r\n<p>we may update our Terms and Conditions from time to time. Thus, you are advised to review this page periodically for any changes. we will notify you of any changes by posting the new Terms and Conditions on this page.</p>\r\n\r\n<p>These terms and conditions are effective as of 2020-05-26</p>\r\n\r\n<p><strong>Contact Us</strong></p>\r\n\r\n<p>If you have any questions or suggestions about our Terms and Conditions, do not hesitate to contact us at CSCODETECH.</p>\r\n', '$', 300, 'rzp_test_Zl4msTIgix9C73');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE `tbl_order` (
  `id` int(11) NOT NULL,
  `pid` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uid` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `aid` int(11) DEFAULT NULL,
  `type` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('pending','processing','completed') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `odate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `template`
--

CREATE TABLE `template` (
  `id` int(11) NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `template`
--

INSERT INTO `template` (`id`, `title`, `message`, `url`) VALUES
(4, 'New Product Alert ! ! ! Limited time', 'Hurry ! Our new product out now. you can buy !!', 'data/2.png');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`id`, `name`, `message`, `status`) VALUES
(1, 'Jhon Deo', 'Really good app, Easy to use, good customer support, never faced any issues from app or customer support, Much appreciated. It seems fish&meat customer support members are very polite and friendly to the customers. Based on this experience I understood having well trained staffs and proper process.', 1),
(2, 'Lisa Joseph', 'Just love it. With essentials added to Fish&meat\'s itinerary, it has now become more useful in our daily lives. And food, that has been always on time and delicious. Also I would like to request Fish&meat\'s to add an option to tip the delivery executive after the order is delivered', 1),
(3, 'Rose Lossy', 'The app is good for ordering food from a variety of restaurants with good price range spread and quality if service. Search can be upgraded to accommodate finer points in search for particular dishes or restaurants, but it does the intended at the moment.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `uread`
--

CREATE TABLE `uread` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `nid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `noti`
--
ALTER TABLE `noti`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `register`
--
ALTER TABLE `register`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `template`
--
ALTER TABLE `template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uread`
--
ALTER TABLE `uread`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `noti`
--
ALTER TABLE `noti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `register`
--
ALTER TABLE `register`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_order`
--
ALTER TABLE `tbl_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `template`
--
ALTER TABLE `template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `uread`
--
ALTER TABLE `uread`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
